1) Run server.exe
2) go to http://localhost:8112/ in your browser (Chrome has full support)
3) Add some graphs
4) Start a Warthunder game
5) Click Start
6) Bask in the glory of visualised data (or report a bug)


== Changelog ==
* BUGFIX: Fixed issue with using 127.0.0.1 instead of localhost
* BUGFIX: Fix saving
* BUGFIX: Check previous save exists before trying to load it
* Auto save graphs between planes/maps
* Refresh graphs when entering new plane/new map
* Retry when connection fails

== Notes ==
Server.exe is a compiled python script. If you like, you can run build/wt.py instead, if you have python 3.3 installed. The server is required in order to proxy the warthunder data through to the wt-graphs page. <tech>Due to broswers preventing cross-site-scripting, and two ports on the same localhost being considered different domains, the wt-graphs page cannot access the warthunder 'api' directly. Instead we need to create a server which requests the warthunder pages and serves them up on a different port.</tech>

There may be data served by warthunder that's not available in the graph dropdowns. I believe the data served up is dependent on the plane being flown. So I welcome the community either by submitting a pull request to this project (https://bitbucket.org/dtownsend/warthunder-live-graphs) or just letting me know what's missing (david@spookstudio.com).

All other bug reports and contributions are welcome, and feel free to fork the project and distribute freely.

== Author ==
David Townsend
Brighton, UK
Web Developer and general hackinator.
david@spookstudio.com

== Licence ==
http://www.dbad-license.org/

DON'T BE A DICK PUBLIC LICENSE

Version 1, December 2009

Copyright (C) 2009 Philip Sturgeon email@philsturgeon.co.uk

Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.

DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

Do whatever you like with the original work, just don't be a dick.

Being a dick includes - but is not limited to - the following instances:

1a. Outright copyright infringement - Don't just copy this and change the name.
1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a dick.
1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER dick.

If you become rich through modifications, related works/services, or supporting the original work, share the love. Only a dick would make loads off this work and not buy the original works creator(s) a pint.

Code is provided with no warranty. Using somebody else's code and bitching when it goes wrong makes you a DONKEY dick. Fix the problem yourself. A non-dick would submit the fix back.